/*!
@file
Defines `boost::hana::detail::dispatch_common`.

@copyright Louis Dionne 2014
Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
 */

#ifndef BOOST_HANA_DETAIL_DISPATCH_COMMON_HPP
#define BOOST_HANA_DETAIL_DISPATCH_COMMON_HPP



#endif // !BOOST_HANA_DETAIL_DISPATCH_COMMON_HPP
