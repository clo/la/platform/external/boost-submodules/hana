/*!
@file
Forward declares the @ref group-core module.

@copyright Louis Dionne 2014
Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
 */

#ifndef BOOST_HANA_FWD_CORE_HPP
#define BOOST_HANA_FWD_CORE_HPP

//! @defgroup group-core Core
//! Core utilities of the library.

//! @defgroup group-typeclasses Type classes
//! General purpose type classes provided by the library.

#error "include good stuff"

#endif // !BOOST_HANA_FWD_CORE_HPP
